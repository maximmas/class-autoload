<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function($classname) {
      // $dir = new DirectoryIterator( __DIR__ . "/classes/pages/" );
      // foreach ( $dir as $index => $file ){
      //   echo $index . ' -> ' . $file->getFilename() . "</br>";
      //   if ( $file->isFile() ){
      //       require_once( __DIR__ . "/classes/pages/" . $file->getFilename() );
      //   };
      // };
      $namespace = explode('\\', $classname);
      $classname = array_pop($namespace);
      $namespace = join('\\', $namespace);
      if ($namespace === 'M12') {
        $file =  __DIR__ . "/classes/pages/" . $classname . ".php";
        echo $file . "<br>" . PHP_EOL;
        require_once($file);
      }
});


M12\Page1::test_self();
M12\Page1::test();
M12\Page2::test_self();
M12\Page2::test();


